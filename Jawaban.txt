Soal 1 Membuat database
create database myshop;

Soal 2 Membuat table di dalam database
Table users
create table users(
    -> id int(10) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table categories
create table categories(
    -> id int(10) primary key auto_increment,
    -> name varchar(255)
    -> );

Table items
create table items(
    -> id int(10) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(15),
    -> stock int(15),
    -> category_id int(10),
    -> foreign key (category_id) references categories(id)
    -> );

Soal 3 Memasukkan data pada table
Table users
insert into users(name,email,password) values ("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

Table categories
insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

Table items
insert into items(name,description,price,stock,category_id) values ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

Soal 4 Mengambil data dari Database
a Mengambil data users
select id,name, email from users;

b. Mengambil data items
   1. select * from items where price>1000000;
   2. select * from items where name like '%sang%';

c. Menampilkan data items join dengan kategori

select items.name,items.description,items.price,items.stock,items.category_id,categories.name as kategori from items inner join categories on items.category_id=categories.id;

Soal 5 Mengubah Data dari Database
update items set price = 2500000 where id = 1;




